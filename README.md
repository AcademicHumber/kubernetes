# kubernetes

## Getting started

Install a gitlab agent on kubernetes.

```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com
```
